package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (expr == null || expr.length() == 0) return null;

        String er [] = {"+-", "--", "*-", "/-"};

        for (String e : er)
            if (expr.contains(e)) return  null;

        double result;
        int comma;

        Counter s = new Director(expr);
        String str = s.Pars();

        if (str == null) return null;
        result = Double.parseDouble(str);
        str = String.format("%.4f",result);
        comma = str.indexOf(',');
        str = str.substring(0,comma)+'.'+str.substring(comma+1);
        for (; ;){
            if (str.lastIndexOf('0') == str.length()-1)
                str = str.substring(0,str.lastIndexOf('0'));
            else if (str.length()-1==comma){
                str = str.substring(0,comma);
                break;
            }
            else break;
        }
        return str;
    }

}
