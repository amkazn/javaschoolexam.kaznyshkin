package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        for (int i = 2, n=1; n<=items.size(); i++ ) {
            n += i;
            try {
                if (n == items.size()) return build(items);
            } catch (Error exc) {break;}
        }
        throw new CannotBuildPyramidException ();
    }
    
    private int [][] build (List<Integer> list){

        int[][] pyr, q = new int [2][];
        ArrayList<Integer> items = new ArrayList<Integer>();
        int  l=0;

        for (int i = 0; i < list.size(); i++)
            items.add(i,list.get(i));

        qsort(items);

        for (int i = 0, k = 0; k < items.size(); i++) {

            for (;;) {
                try {
                    q[i] = new int[i + 1];
                    break;
                } catch (ArrayIndexOutOfBoundsException exc) {
                    int[][] t = new int[q.length * 2][];
                    for (int n = 0; n < q.length; n++) {
                        t[n] = new int[q[n].length];
                        for (int m = 0; m < q[n].length; m++)
                            t[n][m] = q[n][m];
                    }
                    q = t;
                }
            }

            for (int j = 0; j<q[i].length; k++, j++) {
                q[i][j] = items.get(k);
            }
            l = i;
        }

        pyr = new int[l+1][2*q[l].length-1];

        for (int i = pyr.length - 1, k = 0; i >= 0; i--, k++) {
            for (int j = k, z = 0; (j < pyr[i].length) && (z < q[i].length); z++, j += 2) {
                pyr[i][j] = q[i][z];
            }
        }

        return pyr;
    }

    static void  qsort (ArrayList<Integer> List) {

        qs (List, 0, List.size()-1);

    }
    private static void qs(ArrayList<Integer> List, int left, int right) {

        int i, j;
        int x, y;

        i = left; j = right;
        x = List.get((left+right)/2);

        do {

            while ((List.get(i) < x) && (i < right)) i++;
            while ((x < List.get(j)) && (j > left)) j--;

            if (i <= j) {

                y = List.get(i);

                List.add(i, List.get(j));
                List.remove(i+1);
                List.add(j,y);
                List.remove(j+1);

                i++; j--;
            }
        } while(i <= j);

        if (left < j) qs(List, left, j);
        if (i < right) qs(List, i ,right);
    }


}
