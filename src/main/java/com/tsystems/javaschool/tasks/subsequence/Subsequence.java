package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List<? extends Object> x, List<? extends Object> y) {
        // TODO: Implement the logic here
        if (x.isEmpty() || x.size()>y.size())
            return false;
        for (int i = 0, k = 0; i < x.size(); i++){

            Object _x = x.get(i);

            for (int j = k; ;j++){

                if (j > y.size()-1) return false;

                Object _y = y.get(j);
                if(_x.equals(_y)) {
                    k = j+1;
                    break;
                }

            }
        }
        return true;    
    }
}
